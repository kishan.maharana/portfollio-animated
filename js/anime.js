anime({
    targets: '.function-based-values-demo .el',
    translateX: {
        value: 100,
        duration: 800
    },
    rotate: {
        value: 360,
        duration: 1800,
        easing: 'easeInOutSine'
    },
    scale: {
        value: 1.5,
        duration: 1600,
        delay: 800,
        easing: 'easeInOutQuart'
    },
    delay: 250 // All properties except 'scale' inherit 250ms delay
});
anime({
    targets: '.box.el1',
    translateX: {
        value: 300,
        duration: 800
    },
    rotate: {
        value: 360,
        duration: 1800,
        easing: 'easeInOutSine'
    },
    scale: {
        value: 1.5,
        duration: 1600,
        delay: 800,
        easing: 'easeInOutQuart'
    },
    delay: 250 // All properties except 'scale' inherit 250ms delay
});
anime({
    targets: '.box.el2',
    translateX: {
        value: 500,
        duration: 800
    },
    rotate: {
        value: 360,
        duration: 1800,
        easing: 'easeInOutSine',
    },
    scale: {
        value: 1.5,
        duration: 1600,
        delay: 800,
        easing: 'easeInOutQuart',
    },
    delay: 250 // All properties except 'scale' inherit 250ms delay
});