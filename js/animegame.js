setTimeout(function() {
    anime({




        targets: '.el',
        translateX: function(el, i) {
            return 40 + 100 * i;
        },
        scale: function(el, i) {
            return 1 - i * 1.5 + i / 9;
        },
        rotate: function() {
            return anime.random(-180, 180);
        },
        borderRadius: function(el) {
            return Math.random() * el.offsetWidth / 2;
        },
        duration: function() { return anime.random(1500, 2400); },
        delay: function() { return anime.random(0, 1000); },
        autoplay: true
    });
}, 3000);